﻿using System;
using System.IO;
using System.Text;

using static IPR1.EpsonFX800;

namespace IPR1
{
    class Program
    {
        static void Main(string[] args)
        {
            var p1 = Mode.PROPORTIONAL;
            var p2 = Mode.NLQ;
            var p3 = Mode.DOUBLE_WIDTH;
            var p4 = Mode.DOUBLE_HEIGHT;
            var p51 = Mode.DOUBLE_HEIGHT;
            var p52 = Mode.UNDERLINING;
            var p6 = Mode.EMPHASIS;
            var p7 = Font.PICA;
            var p8 = Font.ELITE;
            var p9 = Mode.PROPORTIONAL;
            var p10 = Mode.DOUBLE_WIDTH;
            var p11 = Mode.NLQ;

            var h1 = 1.5F;
            var h2 = 3F;
            var h3 = 2.5F;
            var h4 = 2.5F;
            var h5 = 5;
            var h6 = 3;

            var userCharacter = new byte[] { 0, 48, 24, 12, 6, 255, 255, 6, 12, 24, 48, 0 };

            Action<Printer> graphicsDrawer = (printer) => 
            {
                printer.Write(new byte[] { 27, (byte)'Y', 17, 0 });
                printer.Write(new byte[] { 0 });
                for (var j = 0; j < 5; j++)
                {
                    printer.Write(new byte[] { 4, 252, 4 });
                }
                printer.Write(new byte[] { 0 });
            };

            new Printer()
                .Start("output.bin")
                .Reset()
                .SetAbsoluteLeftPosition(h1).SelectControl(p1).Write("МИНИСТЕРСТВО ОБРАЗОВАНИЯ РЕСПУБЛИКИ БЕЛАРУСЬ").CancelControl(p1).NewLine()
                .NewLine()
                .SetLeftMargin(23).SelectControl(p2).Write("БГУИР").CancelControl(p2)
                .NewLineWithN72Interval(h2)
                .DefineUserCharacter('U', userCharacter)
                .SelectControl(Mode.USER_DEFINED_SET).Write("UUUUUUUUUUUUUUUUUUUU").CancelControl(Mode.USER_DEFINED_SET).NewLine()
                .SetLeftMargin(20)
                .SelectControl(p3).Write("ОТЧЁТ").CancelControl(p3)
                .NewLineWithN72Interval(h3)
                .SetLeftMargin(5)
                .SelectControl(p4).Write("ПО ИНДИВИДУАЛЬНОЙ ПРАКТИЧЕСКОЙ РАБОТЕ №1").CancelControl(p4).NewLine()
                .NewLine()
                .SetLeftMargin(20)
                .SelectControl(p51).Write("курс - ").CancelControl(p51).SelectControl(p52).Write("АОКТ").CancelControl(p52)
                .NewLineWithN72Interval(h4)
                .SelectControl(p6).Write("Студент").CancelControl(p6).SetAbsoluteLeftPosition(h5).SelectControl(p7).Write("Проверил").CancelControl(p7).NewLine()
                .SelectControl(p8).Write("Михайлов Р.И.").CancelControl(p8).SetAbsoluteLeftPosition(h5).SelectControl(p9).Write("Леванцевич В.А.").CancelControl(p9).NewLine().NewLine()
                .CallWithPrinter(graphicsDrawer)
                .CallWithPrinter(graphicsDrawer)
                .CallWithPrinter(graphicsDrawer)
                .NewLineWithN72Interval(h6)
                .SetLeftMargin(20)
                .SelectControl(p10).Write("МИНСК").CancelControl(p10).NewLine()
                .SetLeftMargin(25)
                .SelectControl(p11).Write("2016").CancelControl(p11)
                .NewLine()
                .Finish();
        }
    }

    public static class EpsonFX800
    {
        public interface IControl
        {
            byte[] SelectingSequence { get; }
            byte[] CancelingSequence { get; }
        }

        public sealed class Font : IControl
        {
            public static readonly Font PICA = new Font(3, new byte[] { 27, (byte)'P' }, new byte[] { 27, (byte)'P' }); // For backward compatibility.
            public static readonly Font ELITE = new Font(4, new byte[] { 27, (byte)'M' }, new byte[] { 27, (byte)'P' });
            public static readonly Font CONDENSED = new Font(6, new byte[] { 27, 15 }, new byte[] { 27, (byte)'P' });

            /// <summary>
            /// Chars per inch
            /// </summary>
            public int CPCM { get; private set; }

            /// <summary>
            /// Enabling control.
            /// </summary>
            public byte[] SelectingSequence { get; private set; }
            public byte[] CancelingSequence { get; private set; }

            private Font(int cpCM, byte[] selectingSequence, byte[] cancelingSequence)
            {
                CPCM = cpCM;
                SelectingSequence = selectingSequence;
                CancelingSequence = cancelingSequence;
            }
        }

        public sealed class Mode : IControl
        {
            public static readonly Mode EMPHASIS = new Mode(new byte[] { 27, (byte)'E' }, new byte[] { 27, (byte)'F' });
            public static readonly Mode DOUBLE_STRIKE = new Mode(new byte[] { 27, (byte)'G' }, new byte[] { 27, (byte)'H' });
            public static readonly Mode PROPORTIONAL = new Mode(new byte[] { 27, (byte)'p', 1}, new byte[] { 27, (byte)'p', 0});
            public static readonly Mode UNDERLINING = new Mode(new byte[] { 27, (byte)'-'}, new byte[] { 27, (byte)'-'});
            public static readonly Mode SUPERSCRIPT = new Mode(new byte[] { 27, (byte)'S', 0 }, new byte[] { 27, (byte)'T' });
            public static readonly Mode SUBSCRIPT = new Mode(new byte[] { 27, (byte)'S', 1 }, new byte[] { 27, (byte)'T' });
            public static readonly Mode DOUBLE_HEIGHT = new Mode(new byte[] { 27, 119, 1 }, new byte[] { 27, 119, 0 });
            public static readonly Mode DOUBLE_WIDTH = new Mode(new byte[] { 27, (byte)'W', 1 }, new byte[] { 27, (byte)'W', 0 });
            public static readonly Mode NLQ = new Mode(new byte[] { 27, (byte)'x', 1 }, new byte[] { 27, (byte)'x', 0 });
            public static readonly Mode USER_DEFINED_SET = new Mode(new byte[] { 27, 37, 1 }, new byte[] { 27, 37, 0 });
            

            public byte[] SelectingSequence { get; private set; }
            public byte[] CancelingSequence { get; private set; }

            private Mode(byte[] selectingSequence, byte[] cancelingSequence)
            {
                SelectingSequence = selectingSequence;
                CancelingSequence = cancelingSequence;
            }

        }

        public class Printer
        {
            BinaryWriter output;
            Encoding encoding;

            public Printer Start(string fileName)
            {
                if (null != output)
                {
                    throw new InvalidOperationException("Already started");
                }
                try
                {
                    FileStream outputStream = new FileStream(fileName, FileMode.Create, FileAccess.ReadWrite);
                    output = new BinaryWriter(outputStream);
                    encoding = Encoding.GetEncoding(1251);
                }
                catch (Exception e)
                {
                    output = null;
                    encoding = null;
                    throw new InvalidOperationException("Unable to create output file.", e);
                }
                return this;
            }

            public Printer Reset()
            {
                outputControl('@');
                return this;
            }

            public Printer SelectControl(IControl control)
            {
                Write(control.SelectingSequence);
                return this;
            }

            public Printer CancelControl(IControl control)
            {
                Write(control.CancelingSequence);
                return this;
            }

            public Printer Write(string text)
            {
                return Write(encoding.GetBytes(text));
            }

            public Printer Write(byte[] bytes)
            {
                assertStarted();
                output.Write(bytes);
                return this;
            }

            public Printer Write(char b)
            {
                assertStarted();
                output.Write(b);
                return this;
            }

            public Printer SetAbsoluteLeftPosition(float centimeters)
            {
                int m = (int)Math.Round(centimeters / 2.54 * 60);
                byte n1 = (byte)(m % 256);
                byte n2 = (byte)(m / 256);
                outputControls(36, n1, n2);
                return this;
            }

            public Printer SetLeftMargin(byte columns)
            {
                assertStarted();
                outputControls('l', columns);
                return this;
            }

            public Printer NewLine()
            {
                assertStarted();
                Write("\r\n");
                return this;
            }

            public Printer SetN72LineInterval(float centimeters)
            {
                var n = (byte)((centimeters / 2.54) * 72);
                outputControls('A', n);
                return this;
            }

            public Printer SetN216LineInterval(float centimeters)
            {
                var n = (byte)((centimeters / 2.54) * 216);
                outputControls(51, n);
                return this;
            }

            public Printer NewLineWithN72Interval(float centimeters)
            {
                return SetN72LineInterval(centimeters).NewLine().SetDefaultLineInterval();
            }

            public Printer SetImmediateN216LineFeed(float centimeters)
            {
                var n = (byte)((centimeters / 2.54) * 216);
                outputControls('J', n);
                return this;
            }

            public Printer SetDefaultLineInterval()
            {
                outputControls('A', 12);
                return this;
            }

            public Printer CopyROMIntoRAM()
            {
                Write(new byte[] { 27, 58, 0, 0, 0 });
                return this;
            }

            public Printer SelectUserDefinedChars(char first, char last)
            {
                Write(new byte[] { 27, 38, 0, (byte)first, (byte)last });
                return this;
            }

            public Printer DefineUserCharacter(char userChar, byte[] bytes)
            {
                return CopyROMIntoRAM().
                    SelectUserDefinedChars(userChar, userChar).
                    Write(bytes);
            }

            public Printer CallWithPrinter(Action<Printer> writer)
            {
                writer(this);
                return this;
            }

            public Printer Finish()
            {
                if (null == output)
                {
                    throw new InvalidOperationException("Not started");
                }
                output.Close();
                output = null;
                return this;
            }

            private void outputControl(byte control)
            {
                assertStarted();
                output.Write(new byte[] { 27, control });
                
            }

            private void outputControl(char control)
            {
                outputControl((byte)control);
            }

            private void outputControls(byte control1, params byte[] rest)
            {
                assertStarted();
                output.Write(new byte[] { 27, control1});
                if (null != rest && 0 != rest.Length)
                {
                    output.Write(rest);
                }
            }

            private void outputControls(char control1, params byte[] rest)
            {
                outputControl((byte)control1);
                if (null != rest && 0 != rest.Length)
                {
                    output.Write(rest);
                }
            }

            private void assertStarted()
            {
                if (null != output)
                {
                    return;
                }
                throw new InvalidOperationException("Printer helper not started. Use Start(string).");
            }

        }

    }

    
}
